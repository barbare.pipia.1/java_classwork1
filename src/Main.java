import java.util.*;

public class Main {
    public static void main(String[] args) {

        /*   1.გამოიტანეთ დაპროგრამების ენების სახელები
        ცალ–ცალკე ხაზზე( C++,  C#,  java, pascal, php, JavaScript, ActionScript).
         */
//        System.out.println("C++");
//        System.out.println("C#");
//        System.out.println("Java");
//        System.out.println("pascal");
//        System.out.println("php");
//        System.out.println("JavaScript");
//        System.out.println("ActionScript");

        /* 2.	დავწეროთ კოდი რომელიც გამოიტანს შეტანილი ორი მთელი რიცხვის შემთხვევაში,
         პირველის მეორეზე გაყოფის შედეგად მიღებულ მთელ შედეგს და
         მეორის პირველზე გაყოფის შედეგად მიღებულ ნაშთს. (ფორმატის გათვალისწინებით).
         */

//        Scanner numb = new Scanner(System.in);
//        System.out.println("შეიყვანეთ პირველი რიცხვი: ");
//        int numb1 = numb.nextInt();
//        System.out.println("შეიყვანეთ მეორე რიცხვი: ");
//        int numb2 = numb.nextInt();
//        System.out.println("გაყოფის მთელი შედეგია:" + (numb1/numb2));
//        System.out.println("ნაშთია:" + (numb1%numb2));


        /* 3.	შეიტანეთ სამი მთელი რიცხვი, გამოიტანეთ მათი ჯამი და ნამრავლი.*/
//        Scanner numb = new Scanner(System.in);
//        System.out.println("შეიყვანეთ პირველი რიცხვი: ");
//        int numb1 = numb.nextInt();
//        System.out.println("შეიყვანეთ მეორე რიცხვი: ");
//        int numb2 = numb.nextInt();
//        System.out.println("შეიყვანეთ მესამე რიცხვი: ");
//        int numb3 = numb.nextInt();
//        System.out.println("სამი რიცხვის ჯამია:"+ (numb1+numb2+numb3));
//        System.out.println("სამი რიცხვის ნამრავლია: "+ (numb1*numb2*numb3));

        /*4.	შეიტანეთ სამნიშნა მთელი რიცხვი,
         დაბეჭდეთ  რიცხვის ციფრები ცალ-ცალკე ხაზზე.
         */

//
//        Scanner number = new Scanner(System.in);
//        System.out.println("შეიყვანეთ სამნიშნა რიცხვი: ");
//        int num = number.nextInt();
//        int num1 = num/100;
//        int num2 = (num % 100)/10;
//        int num3 = num %10;
//        System.out.println(num1);
//        System.out.println(num2);
//        System.out.println(num3);
//
//        }

        /*5.შეიტანეთ ოთხნიშნა მთელი რიცხვი, დაბეჭდეთ რიცხვის ციფრთა ჯამი.
         */
//        Scanner number = new Scanner(System.in);
//        System.out.println("შეიყვანეთ ოთხნიშნა რიცხვი: ");
//        int num = number.nextInt();
//        int num1 = num / 1000;
//        int num2 = (num % 1000) / 100;
//        int num3 = (num % 100) /10;
//        int num4 = num % 10;
//        System.out.println("ციფრთა ჯამია: " + (num1+num2+num3+num4));
//    }


    /*6.	შეიტანეთ ნებისმიერი მთელი რიცხვი, დაბეჭდეთ რიცხვის ციფრთა ჯამი. */
//        int number, digit, sum = 0;
//        Scanner sc = new Scanner(System.in);
//        System.out.println("შეიყვანეთ რიცხვი: ");
//        number = sc.nextInt();
//        while (number > 0) {
//            digit = number % 10;
//            sum = sum + digit;
//            number = number / 10;
//        }
//        System.out.println("ციფრთა ჯამია: " + sum);

        /* 7.	იპოვეთ ორი მთელი რიცხვის უდიდესი
         საერთო გამყოფი და უმცირესი საერთო ჯერადი.(ევკლიდეს ალგორითმი).
         */
//        Scanner numb = new Scanner(System.in);
//        System.out.println("შეიყვანეთ პირველი რიცხვი: ");
//        int numb1 = numb.nextInt();
//        System.out.println("შეიყვანეთ მეორე რიცხვი: ");
//        int numb2 = numb.nextInt();
//        System.out.println();



        /*9.	განსაზღვრეთ 8 ელემენტიანი ნამდვილ რიცხვთა მასივი,
         დაბეჭდეთ მასივის ელემენტები, მასივის უდიდესი და უმცირესი ელემენტი.
         */

//          Integer [] a = {3,4,6,7,8,9,2,3};
//        System.out.println("მასივის ელემენტებია: ");
//          for (int i=0; i < a.length; i++){
//              System.out.println(a[i]);
//
//        }
//          int min = Collections.min(Arrays.asList(a));
//          int max = Collections.max(Arrays.asList(a));
//
//        System.out.println("მასივის უმცირესი რიცხვია:" + min);
//        System.out.println("მასივის უდიდესი რიცხვია:" + max);

        /*10.	განსაზღვრეთ 8 ელემენტიანი ნამდვილ რიცხვთა მასივი,
         დაალაგეთ მასივი ზრდადობით, დაბეჭდეთ მასივი.
         */

//        int [] a = new int[] {3,4,6,7,8,9,2,6};
//        Arrays.sort(a);
//        System.out.println("მასივი ზრდადობის მიხედვით: ");
//        for (int i = 0; i < a.length; i++ ){
//            System.out.println(a[i]);
//        }


        /*11.	განსაზღვრეთ 8 ელემენტიანი ნამდვილ რიცხვთა მასივი,
         ჩაწერეთ მასში შემთხვევითი რიცხვები, დაბეჭდეთ მასივი.
         */

//        Scanner sc = new Scanner(System.in);
//        System.out.println("შეიყვანეთ მასივის სიგრძე: ");
//        int size = sc.nextInt();
//        int [] a = new int [size];
//        System.out.println("შემთხვევითი რიცხვებია: ");
//        for (int i = 0; i < a.length; i++){
//            int random_numb = (int)(Math.random() * 10);
//            a[i] = random_numb;
//            System.out.println(a[i]);
//
//        }
        /*12.	განსაზღვრეთ 8 ელემენტიანი მთელ რიცხვთა მასივი,
         ჩაწერეთ მასში შემთხვევითი რიცხვები, დაალაგეთ მასივი კლებადობით, დაბეჭდეთ მასივი.
         */



    }

}

